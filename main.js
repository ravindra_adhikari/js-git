   
const API_URL = "https://api.github.com/search/repositories?q=language:javascript&sort=star&order=desc&page=";

var app = new Vue({
    el : '#app',

    data:{
        loading: false,
        jsonData : null,
        pageNo :0,
        currentSelected: 0, 
    },
    methods : {
        // using axios to get data
       getDatas(pageNo){       
        pageNo = this.pageNo + 1; 
        console.log(pg + 'pg');
        this.loading = true;
        axios.get(API_URL+pg)
        .then((response)  =>  {
          this.loading = false;
          this.jsonData = response.data.items;  // geting only items from api is needed.
        }, (error)  =>  {
          this.loading = false;
        })
      },
        updateDetails (index) {
            this.currentSelected = index;
            shouldFlash = true;
        },
        nextPage(){
            this.pageNo++;
            this.getDatas(this.pageNo);
            
        },
        previousPage(){
                this.pageNo--;
                this.getsDatas(this.pageNo);
        }
    },
    computed: {
        ownerName(){
            return this.jsonData[this.currentSelected].owner.login;
         },
         watchers(){
             return this.jsonData[this.currentSelected].watchers;
            },
        gitUrl(){
                return this.jsonData[this.currentSelected].html_url;
         },
        downloadRepo(){
                return this.jsonData[this.currentSelected].downloads_url;
         },
         fullName(){
            return  this.jsonData[this.currentSelected].name;   // heres still a TypeError 
            },
    },

    mounted(){
       this.getDatas();
    }
})